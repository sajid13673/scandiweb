<?php
session_start();
?>
<html>

<head>
  <style>
    .addForm {
      Width: 600px;
      margin: auto;
      border: 2px solid dimgray;
      padding: 20px;
      border-radius: 15px;
      font-weight: bold;
      font-family: "Verdana" ;
    }

    input {
      border-radius: 5px;
    }
  </style>

</head>

<body>
  <script>
    function changeOptions(selectEl) {
      let selectedValue = selectEl.options[selectEl.selectedIndex].value;
      let optionFurniture = document.getElementById("Furniture");
      let optionDVD = document.getElementById("DVD");
      let optionBook = document.getElementById("Book");

      if (selectedValue === "Furniture") {
        optionFurniture.style.display = "block";
        optionDVD.style.display = "none";
        optionBook.style.display = "none";


      } else if (selectedValue === "DVD") {
        optionFurniture.style.display = "none";
        optionDVD.style.display = "block";
        optionBook.style.display = "none";


      } else if (selectedValue === "Book") {
        optionFurniture.style.display = "none";
        optionDVD.style.display = "none";
        optionBook.style.display = "block";


      }

    }

    function inputvalidation() {
      let typeSwitcher = document.getElementById("productType");
      let selectedType = typeSwitcher.options[typeSwitcher.selectedIndex].value;
      console.log(selectedType);
      let SKUInput = document.getElementById("sku").value;
      let nameInput = document.getElementById("name").value;
      let priceInput = document.getElementById("price").value;
      let heightInput = document.getElementById("height").value;
      let widthInput = document.getElementById("width").value;
      let lengthInput = document.getElementById("length").value;
      let weightInput = document.getElementById("weight").value;
      let sizeInput = document.getElementById("size").value;

      if (SKUInput == "") {
        alert("Please enter the SKU");
        return false;
      } else if (nameInput == "") {
        alert("Please enter name");
        return false;
      } else if (priceInput == "") {
        alert("Please enter price");
        return false;

      } else if (isNaN(priceInput)) {
        alert("Please, provide a numeric value for price");
        return false;
      }
      while (selectedType === "DVD") {
        if (sizeInput == "") {
          alert("Please enter size");
          return false;
        } else if (isNaN(sizeInput)) {
          alert("Please, provide a numeric value for size");
          return false;
        } else {
          return true;
        }
      }

      while (selectedType === "Book"){
      if (weightInput == "") {
        alert("Please enter weight");
        return false;}
        else if (isNaN(weightInput)){
          alert("Please, provide a numeric value for weight");
          return false;
        }
        else{return true;}
      }
      while (selectedType === "Furniture"){ 
        if (heightInput == "") {
        alert("Please enter height");
        return false;
      }
      else if(isNaN(heightInput)){
        alert("Please, provide a numeric value for height");
          return false;
      }
      else if(widthInput == ""){
        alert("Please enter width");
        return false;
      }
      else if(isNaN(widthInput)){
        alert("Please, provide a numeric value for width");
          return false;
      }
      else if (lengthInput == ""){
        alert("Please enter length");
        return false;
      }
      else if (isNaN(lengthInput)){
        alert("Please, provide a numeric value for length");
          return false;
      }
    else {return true;}

    }

      while (selectedType === "Furniture" && lengthInput == "") {
        alert("Please enter dimensions");
        return false;
      }

    }

    
  </script>

  <?php

  include 'dbconn.php';
  if (isset($_SESSION['add-btn'])) {
    $myMessage = addslashes($_SESSION["add-btn"]);
    echo "<script type='text/javascript'>alert('$myMessage');</script>";
    unset($_SESSION['add-btn']);
  }
  ?>
  <form action="library/addProductsmp.php" method="POST" id="product_form">
  <?php
  include 'library/addProductHeader.php'
  ?>
  <div class="addForm">

      SKU :
      <br>
      <input type="text" name="sku" id="sku">
      <br>
      Product Name :
      <br>
      <input type="text" name="name" id="name">
      <br>
      Price ($):
      <br>
      <input type="decimal" min="0.00" name="price" id="price">
      <br>
      Product type :
      <br>
      <select name="productType" id="productType" onchange="changeOptions(this)">
        <option value="" selected="selected"></option>
        <option value="Furniture">Furniture</option>
        <option value="Book">Book</option>
        <option value="DVD">DVD</option>

      </select>
      <br>
      <br>
      <div id="Furniture" style="display: none;">
        <br>
        Height (CM):
        <br>
        <input type="decimal" min="0.00" name="height" id="height">
        <br>
        Width (CM):
        <br>
        <input type="decimal" min="0.00" name="width" id="width">
        <br>
        Length (CM):
        <br>
        <input type="decimal" min="0.00" name="length" id="length">
        <br><br>
        Please, provide dimensions in HxWxL
      </div>

      <div id="Book" style="display: none;">
        Weight (KG):
        <input type="decimal" name="weight" id="weight">
        <br><br>
        Please, provide weight
      </div>

      <div id="DVD" style="display: none;">
        Size (MB):
        <input type="decimal" min="0.00" name="size" id="size">
        <br><br>
        Please, provide size
      </div>

      <br>


    
  </div>
  </form>
  <?php


  include 'library/footer.php';
  ?>


</body>

</html>