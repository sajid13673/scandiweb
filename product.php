
<?php

 abstract class product{
     private $SKU;
     private $name;
     private $price;
     private $productType;

     function __construct ($SKU, $name, $price, $productType){
        $this->SKU = $SKU;
        $this->name = $name;
        $this->price = $price;
        $this->productType = $productType;

     }
   public function setSKU ($SKU){
    $this->SKU = $SKU;
   }
   public function setName ($name){
    $this->name = $name;
   }
   public function setPrice ($price){
    $this->price = $price;
   }
   public function setProductType($productType){
    $this->productType = $productType;
   }
  // abstract public function insertProductDetails($SKU, $name, $price, $productType);

    public function getSKU (){
        return $this->SKU;
    }
    public function getName (){
        return $this->name;
    }
    public function getPrice (){
        return $this->price;
    }
    public function getProductType (){
        return $this->productType;
    }




}

?>