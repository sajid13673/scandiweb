<!DOCTYPE html>
<html lang="en">
<head>
  <title>Product Add</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<style>
    .navbar-brand{
        font-size:40px;
    }
    </style>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header" >
      <a class="navbar-brand">Product Add</a>
    </div>
    <ul class="nav navbar-nav">

    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a ><button class="btn btn-default navbar-btn"  type="submit" class="btn btn-success" onclick=" return inputvalidation();" name="addProduct-btn" id="addProduct-btn"  >Save</button></a></li>
      <li><a ><button class="btn btn-default navbar-btn" type="submit"  name="cancel-btn" id="cancel-btn" value="Cancel">Cancel</button></a></li>
    </ul>
  </div>
</nav>
  


</body>
</html>
