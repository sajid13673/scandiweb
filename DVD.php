<?php

class DVD extends product
{    private $size;

    function __construct($SKU, $name, $price, $productType, $size)
    {
        parent::__construct($SKU, $name, $price, $productType);
        $this->size = $size;
        print "In SubClass constructor\n";
    }

    public function setSize($size)
    {
        $this->size = $size;    }   
    public function getSize()
    {
        return $this->size;    }   
        
    
}

?>