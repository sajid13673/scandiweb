<script>
  function changeOptions(selectEl) {
    let selectedValue = selectEl.options[selectEl.selectedIndex].value;
    let optionFurniture = document.getElementById("Furniture");
    let optionDVD = document.getElementById("DVD");
    let optionBook = document.getElementById("Book");

    if (selectedValue === "Furniture") {
      optionFurniture.style.display = "block";
      optionDVD.style.display = "none";
      optionBook.style.display = "none";
    

    } 
    else if (selectedValue === "DVD") {
      optionFurniture.style.display = "none";
      optionDVD.style.display = "block";
      optionBook.style.display = "none";
    

    } 
    else if (selectedValue === "Book") {
      optionFurniture.style.display = "none";
      optionDVD.style.display = "none";
      optionBook.style.display = "block";
    

    } 

  }
</script>
<div style="Width: 600px; margin: auto; border: 2px dashed dimgray; padding :
20px; border-radius: 15px;">
  <form>
    <select onchange="changeOptions(this)">
      <option value="" selected="selected"></option>
      <option value="Furniture">Furniture</option>
      <option value="Book">Book</option>
      <option value="DVD">DVD</option>

    </select>

    <div id="Furniture" style="display: none;">
      <br>
      Height (CM):
      <br>
      <input type="number" name="height" id="height">
      <br>
      Width (CM):
      <br>
      <input type="number" name="width" id="width">
      <br>
      Length (CM):
      <br>
      <input type="number" name="length" id="length">
    </div>

    <div id="Book" style="display: none;">
      Weight (KG):
      <input type="number" name="weight" id="weight">
    </div>

    <div id="DVD" style="display: none;">
      Size (MB):
      <input type="number" name="size" id="size">
    </div>
    <form>
</div>