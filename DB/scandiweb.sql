-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 01, 2022 at 02:58 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scandiweb`
--

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `SKU` text NOT NULL,
  `productName` text NOT NULL,
  `Price` decimal(10,2) NOT NULL,
  `productType` text NOT NULL,
  `size` decimal(11,0) DEFAULT NULL,
  `height` decimal(8,2) DEFAULT NULL,
  `width` decimal(8,2) DEFAULT NULL,
  `length` decimal(8,2) DEFAULT NULL,
  `weight` decimal(8,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`SKU`, `productName`, `Price`, `productType`, `size`, `height`, `width`, `length`, `weight`) VALUES
('bk1254', 'Modern world', '115.00', 'Book', NULL, NULL, NULL, NULL, '1.20'),
('bk1325', 'Peaceful mind', '67.99', 'Book', NULL, NULL, NULL, NULL, '0.70'),
('bk2337', 'Weight loss', '65.00', 'Book', NULL, NULL, NULL, NULL, '1.50'),
('bk4514', 'Advanced programming', '315.00', 'Book', NULL, NULL, NULL, NULL, '1.30'),
('bk8900', 'Alice in wonderland', '210.00', 'Book', NULL, NULL, NULL, NULL, '0.80'),
('dvd1897', 'Inception', '199.00', 'DVD', '4100', NULL, NULL, NULL, NULL),
('dvd2014', 'Titanic', '220.00', 'DVD', '4005', NULL, NULL, NULL, NULL),
('dvd2529', 'Jurrasic world', '175.00', 'DVD', '3990', NULL, NULL, NULL, NULL),
('dvd3755', 'Minions', '98.00', 'DVD', '3750', NULL, NULL, NULL, NULL),
('fur1588', 'Wooden Desk', '540.00', 'Furniture', NULL, '70.00', '55.00', '110.00', NULL),
('fur1659', 'Steel table', '215.00', 'Furniture', NULL, '60.00', '55.00', '75.00', NULL),
('fur2187', 'Bedside table', '300.00', 'Furniture', NULL, '60.00', '45.00', '45.00', NULL),
('fur4654', 'Wooden table', '150.00', 'Furniture', NULL, '65.00', '60.00', '80.00', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`SKU`(20));
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
