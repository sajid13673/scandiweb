<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <title>home</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <style>
        p {
            text-align: center;
        }

        h2 {
            text-align: center;
        }
        .card{
            border: solid 1px;
            margin: 5px;

        }
    </style>


</head>

<body>

    <?php
    include 'library/footer.php';
    include 'dbconn.php';

    $sql = "SELECT * FROM `product`";
    $result = $connection->query($sql);

    ?>
    <div>
        <?php
        if (isset($_SESSION['status'])) {
            $Message= addslashes($_SESSION["status"]);
            echo "<script type='text/javascript'>alert('$Message');</script>";
            unset($_SESSION['status']);
        }
        if (isset($_SESSION['cancel'])) {
            $myMessage= addslashes($_SESSION["cancel"]);
            echo "<script type='text/javascript'>alert('$myMessage');</script>";
           unset($_SESSION['cancel']);
        }

        function displayProductDetails($row)
        {
            if ($row['productType'] == 'DVD') {
                echo "Size: " . $row['size'], " MB";
            } elseif ($row['productType'] == 'Book') {
                echo "Weight: " . $row['weight'], " KG";
            } elseif ($row['productType'] == 'Furniture') {
                echo "Dimensions: " . $row['height'], "x" . $row['width'], "x" . $row['length'];
            }
        }
        ?>
        <form action="library/deletemp.php" method="POST">
        <?php 
                include 'library/homeHeader.php';
                ?>

        <div class="container py-5">
            
                
              
                <div class="row mt-4">
                    <?php
                    $sql = "SELECT * FROM `product`";
                    $result = $connection->query($sql);
                    if ($result->num_rows > 0)    
                    while ($row = $result->fetch_assoc()) {
                    ?>
                        <div class="col-md-3">
                            <div class="card" >

                                <div class="body">
                                    <input type="checkbox" name="delete-checkbox[]" class="delete-checkbox" value="<?= $row['SKU'] ?>" style="margin: 5px;">
                                    <h2 class="card-title    "><?php echo $row['SKU']; ?></h2>
                                    <p class="card-text"><?php echo $row['productName']; ?><br><?php echo $row['Price'], " $"; ?><br> <?php displayProductDetails($row); ?></p>
                                </div>
                            </div>
                        </div>

                    <?php


                    }
                    ?>




                </div>
            
        </div>
        </form>

    </div>






</body>

</html>